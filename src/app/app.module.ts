import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//Material imports
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';


//prime ng imports
import { ProgressSpinnerModule  } from 'primeng/progressspinner';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import {GMapModule} from 'primeng/gmap';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {GrowlModule} from 'primeng/growl';



import{ FormsModule, ReactiveFormsModule} from '@angular/forms';

//scroll to library
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';


//MyService
import { MyService } from './app.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  
  imports: [
    ScrollToModule,
    ScrollToModule.forRoot(),
    MatToolbarModule,
    MatDividerModule,
    MatGridListModule,
    MatInputModule,
    MatTabsModule,
    ProgressSpinnerModule,
    AccordionModule,
    GMapModule,
    GrowlModule,
    CardModule,
    ButtonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
