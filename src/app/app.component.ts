import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder,FormGroup} from '@angular/forms';
import { trigger,state,style,animate,transition } from '@angular/animations';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

//importing service
import { MyService} from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'STYCH';
  options:any;
  msgs:any=[];
  

  notification(type, summary, detail) {
    this.msgs.push({ severity: type, summary: summary, detail: detail });
  }

  fg: FormGroup;
  s:MyService
 
  constructor(private fb: FormBuilder, ) {
    this.fg = this.fb.group({
      name: ['', Validators.required, Validators.minLength(1)],
      email: ['', Validators.required, Validators.minLength(1), Validators.email],
      message: ['', Validators.required]
    })

    this.options = {
      center: { lat: 28.6279842, lng: 77.3781253 },
      zoom: 12
    };

  

  }


  clickFunc(event) {
    console.log(event);
    console.log(event.tab.textLabel, "=====================");
    if (event.tab.textLabel == "HOW IT WORKS") {
          document.getElementById("how").scrollIntoView({block: 'start', behavior: 'smooth'});
    }
    else if (event.tab.textLabel == "CONTACT US") {
      document.getElementById("contact").scrollIntoView({block: 'start', behavior: 'smooth'});
    }
}


send(){
  this.notification("success","Send Successfully :)","We Will Reach U Soon");
}

}

